[SECTION] INSERTING RECORDS

--inserting data to a particular table and 1 column
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("PSY");

--inserting data to a particular table and/with 2 or more columns
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 253, "OPM", 2);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 253, "OPM", 2);

[SECTION] --READ AND SELECT RECORDS/DATA

-- Display the title and genre of all the songs
SELECT song_name, genre FROM songs;

--Display he song name of all the OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";

--Display the title of all the songs
SELECT * FROM songs;

--Display the title and length of the OPM songs that are more than 2 minutes
SELECT song_name, length FROM songs WHERE length > 200 AND genre = "OPM";

--Display the title and length of the OPM songs that are more than 2:30 minutes
SELECT song_name, length FROM songs WHERE length > 230 AND genre = "OPM";

[SECTION] Updating records 
UPDATE songs SET length = 259 WHERE song_name = "214";

UPDATE songs SET song_name = "Ulan Updated" WHERE song_name = "Ulan"

[SECTION] Deleting records
DELETE FROM songs WHERE genre = "OPM" AND length > 250;

-- delete all from a table
DELETE * FROM songs;